# Self-extracting archive. 

Requirements:

* C++ 11/14 compiler

* Qt framework v4 or v5

* quaZip and zlib (installation instructions you can find [here](http://www.antonioborondo.com/2014/10/22/zipping-and-unzipping-files-with-qt/) )


