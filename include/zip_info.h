#pragma once

/**
 * @brief A common info about zip archive
 */
struct ZipInfo {
    unsigned long long totalSize;
    int filesCount;

    ZipInfo() : totalSize(0), filesCount(0) {}
};
