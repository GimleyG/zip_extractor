#pragma once

#include <QIODevice>
#include <QObject>
#include <QString>

#include "zip_info.h"

/**
 * @brief Extracts data from a zip archive and provides signals of
 *        the extract process state
 */
class Extractor : public QObject {
    Q_OBJECT
public:
    Extractor();

    void extractResourceArchive(const QString &path_to_extract);

signals:
    void extractingFile(const QString &name);
    void gotZipInfo(const ZipInfo &info);
    void unpackedBytes(unsigned int count);
    void finished();

public slots:
    void terminate();

private:
    bool copyData(QIODevice &inFile, QIODevice &outFile);

    volatile bool terminated_;
};

