#pragma once

#include <QMainWindow>
#include <QProgressBar>
#include <QPushButton>
#include <QTextEdit>
#include <QVBoxLayout>

#include "zip_info.h"

/**
 * @brief The MainWindow class represents main window and all its components.
 */
class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    bool isCanceled() { return canceled_; }

signals:
    void canceled();

public slots:
    void onChangeLogArea(const QString &msg);
    void onGotZipInfo(const ZipInfo &info);
    void onUnpackedBytes(unsigned int count);
    void onExtractionFinished();

private slots:
    void onCancelBtnClicked();

private:
    void setupUI();

    unsigned long long int unpackedBytes_;
    ZipInfo info_;
    bool canceled_;

    QWidget *window_;
    QVBoxLayout *layout_;
    QProgressBar *progress_;
    QTextEdit *log_area_;
    QPushButton *cancel_btn_;
};
