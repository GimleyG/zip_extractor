#include "main_window.h"

#include <QApplication>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), unpackedBytes_(0), canceled_(false) {
    setupUI();
    this->setWindowTitle("Extracting game.zip");
    connect(cancel_btn_, SIGNAL(clicked()),this, SLOT(onCancelBtnClicked()));
}

MainWindow::~MainWindow() {
    delete cancel_btn_;
    delete log_area_;
    delete progress_;
    delete layout_;
    delete window_;
}

void MainWindow::onCancelBtnClicked() {
    canceled_ = true;
    emit canceled();
    emit qApp->quit();
}


void MainWindow :: onChangeLogArea(const QString &msg) {
    QTextCursor c =  log_area_->textCursor();
    c.movePosition(QTextCursor::End);
    log_area_->setTextCursor(c);
    emit log_area_->append(msg);
}

void MainWindow::onGotZipInfo(const ZipInfo &info) {
    this->info_ = info;
}

void MainWindow::onUnpackedBytes(unsigned int count){
    unpackedBytes_+= count;
    int newValue = (1.f*unpackedBytes_/info_.totalSize) * 100;
    progress_->setValue(newValue);
    progress_->setFormat(QString::number(newValue) + "%");
    progress_->update();
}

void MainWindow::onExtractionFinished() {
    close();
}

void MainWindow::setupUI() {
    setFixedSize(QSize(500, 300));

    layout_ = new QVBoxLayout;

    progress_ = new QProgressBar();
    progress_->setFixedSize(QSize(450, 20));
    progress_->setTextVisible(true);
    progress_->setMaximum(100);
    layout_->addWidget(progress_, 0, Qt::AlignTop);

    log_area_ = new QTextEdit();
    log_area_->setFixedSize(QSize(480, 220));
    log_area_->setEnabled(false);
    layout_->addWidget(log_area_, 1, Qt::AlignJustify);

    cancel_btn_ = new QPushButton("Cancel");
    cancel_btn_->setGeometry(QRect(QPoint(100, 100), QSize(200, 50)));
    layout_->addWidget(cancel_btn_, 1, Qt::AlignRight | Qt::AlignBottom);

    window_ = new QWidget();
    window_->setLayout(layout_);

    setCentralWidget(window_);
}
