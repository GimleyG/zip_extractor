#include "extractor.h"

#include <QDir>

#include <cstdlib>
#include <numeric>
#include <quazip.h>
#include <quazipfile.h>

static constexpr const char *password = "123";

Extractor::Extractor() : QObject(), terminated_(false){
}

void Extractor::extractResourceArchive(const QString &path_to_extract) {
    QFile f("://game.zip");
    QuaZip zip(&f);
    //according to DOS code page
    zip.setFileNameCodec(QTextCodec::codecForName("IBM 866"));

    zip.open(QuaZip::mdUnzip);
    QuaZipFile zFile(&zip);

    auto lst = zip.getFileInfoList64();
    ZipInfo info;
    info.filesCount = lst.size();
    for(auto l : lst)
        info.totalSize += l.uncompressedSize;    
    emit gotZipInfo(info);

    for(bool has_more=zip.goToFirstFile(); has_more; has_more=zip.goToNextFile()) {
        if(terminated_) break;

        auto fileName = zip.getCurrentFileName();
        emit extractingFile(QString("\"%1\" extracted").arg(fileName));

        QString full_name = path_to_extract + fileName;

        // name of a folder ends with '/'
        if(fileName.at(fileName.length()-1) == "/") {
            QDir().mkpath(full_name);
        } else {
            zFile.open( QIODevice::ReadOnly, password );

            QFile dstFile(full_name);
            dstFile.open( QIODevice::WriteOnly );

            copyData(zFile, dstFile);

            zFile.close();
            dstFile.close();
        }
    }
    zip.close();

    if(terminated_) {
        QDir(path_to_extract).removeRecursively();
    }

    emit finished();
}

void Extractor::terminate() {
    terminated_ = true;
}

bool Extractor::copyData(QIODevice &inFile, QIODevice &outFile) {
    while (!inFile.atEnd()) {
        if(terminated_) break;

        static char buf[4096];
        qint64 readLen = inFile.read(buf, 4096);
        if (readLen <= 0)
            return false;
        if (outFile.write(buf, readLen) != readLen)
            return false;
        else
            emit unpackedBytes(readLen);
    }
    return true;
}
