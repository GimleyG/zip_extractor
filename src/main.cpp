#include <QApplication>
#include <QDir>
#include <QMessageBox>
#include <QTextCodec>

#include <thread>
#include <windows.h>

#include "main_window.h"
#include "extractor.h"

/**
 * @brief Generates random string with a required length
 * @param length: length of a result string
 * @return QString which contains a generated string
 */
static QString generateRandomString(size_t length);

/**
 * @brief Start a standalone process by given name of an executable file
 * @param full name of executable file
 */
static void startSeparatedProcess(const QString &exe);


Q_DECLARE_METATYPE(ZipInfo)

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MainWindow w;
    Extractor *ex = new Extractor();

    qRegisterMetaType<ZipInfo>();
    QObject::connect(ex, SIGNAL(extractingFile(const QString &)),
               &w, SLOT(onChangeLogArea(const QString &)));
    QObject::connect(ex, SIGNAL(gotZipInfo(const ZipInfo &)),
               &w, SLOT(onGotZipInfo(const ZipInfo &)));
    QObject::connect(ex, SIGNAL(unpackedBytes(unsigned int)),
               &w, SLOT(onUnpackedBytes(unsigned int)));
    QObject::connect(ex, SIGNAL(finished()),
               &w, SLOT(onExtractionFinished()));
    QObject::connect(&w, SIGNAL(canceled()),
               ex, SLOT(terminate()));

    w.show();

    QString path_to_extract = QString("%1\\%2\\").arg(std::getenv("TEMP")).arg(generateRandomString(8));
    QDir().mkdir(path_to_extract);

    std::thread thr(&Extractor::extractResourceArchive, ex, path_to_extract);

    a.exec();
    thr.join();
    delete ex;

    if(w.isCanceled()) return 0;

    QString exe = path_to_extract + "game.exe";
    startSeparatedProcess(exe);

    return 0;
}

static QString generateRandomString(size_t length) {
  static const char charmap[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                "abcdefghijklmnopqrstuvwxyz";
  const size_t charmapLength = strlen(charmap);

  auto generator = [&](){ return charmap[rand() % charmapLength]; };
  QString result;
  result.reserve(length);
  std::generate_n(std::back_inserter(result), length, generator);
  return result;
}

static void startSeparatedProcess(const QString &exe) {
    STARTUPINFO siStartupInfo;
    PROCESS_INFORMATION piProcessInfo;

    memset(&siStartupInfo, 0, sizeof(siStartupInfo));
    memset(&piProcessInfo, 0, sizeof(piProcessInfo));

    siStartupInfo.cb = sizeof(siStartupInfo);

    if(QFileInfo(exe).exists()) {
        if(CreateProcess(exe.toStdWString().c_str(), NULL, 0, 0, FALSE,
                        CREATE_DEFAULT_ERROR_MODE, 0, 0, &siStartupInfo, &piProcessInfo))
        {
            CloseHandle(piProcessInfo.hThread);
            CloseHandle(piProcessInfo.hProcess);
        }
    } else {
        QMessageBox::critical(0, "Error", "Unable to run game.exe: file not found");
    }
}
