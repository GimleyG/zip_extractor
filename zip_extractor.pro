QT += core
QT += gui
QT += widgets

CONFIG += c++11
CONFIG += resources_big

DESTDIR = bin
TARGET = zip_extractor

TEMPLATE = app

INCLUDEPATH += include
VPATH += include src
#system(g++ -static -static-libgcc -static-libstdc++ game.cpp -o game.exe )

SOURCES += main.cpp \
    main_window.cpp \
    extractor.cpp

HEADERS += \
    main_window.h \
    extractor.h \
    include/zip_info.h

INCLUDEPATH += $$(ZLIB_HOME)
LIBS += -L$$(ZLIB_HOME) -lz
INCLUDEPATH += $$(QUAZIP_HOME)
LIBS += -L$$(QUAZIP_HOME)/release -lquazip


RESOURCES += \
    archive.qrc

DEFINES += QT_DEPRECATED_WARNINGS
